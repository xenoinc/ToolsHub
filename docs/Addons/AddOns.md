# ToolsHub Extension Points

## Endpoints

| Node Name | Path |
|---|---|
| StartupAddin | ToolsHub/AppStartup |
|  | ToolsHub/Sidebar |
|  | ToolsHub/SystemTray |