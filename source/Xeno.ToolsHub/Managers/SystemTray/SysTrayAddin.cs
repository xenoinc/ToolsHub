﻿/* Copyright Xeno Innovations, Inc. 2018
 * Date:    2018-10-24
 * Author:  Damian Suess
 * File:    SysTrayAddin.cs
 * Description:
 *  SystemTray add-in
 */

using Xeno.ToolsHub.ExtensionModel;

namespace Xeno.ToolsHub.Managers.SystemTray
{
  public class SysTrayAddin : AbstractAddin
  {
    public SysTrayAddin()
    {
      ;
    }
  }
}
