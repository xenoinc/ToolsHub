﻿/* Copyright Xeno Innovations, Inc. 2018
 * Date:    2018-7-24
 * Author:  Damian Suess
 * File:    AddinManagerCtrl.cs
 * Description:
 *  
 */

using System.Windows.Forms;

namespace Xeno.ToolsHub.Views.Preferences
{
  public partial class AddinManagerCtrl : UserControl
  {
    public AddinManagerCtrl()
    {
      InitializeComponent();
    }
  }
}
