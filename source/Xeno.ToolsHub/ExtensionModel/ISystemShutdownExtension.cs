﻿/* Copyright Xeno Innovations, Inc. 2018
 * Date:    2018-7-17
 * Author:  Damian Suess
 * File:    ISystemShutdownExtension.cs
 * Description:
 *
 */

namespace Xeno.ToolsHub.ExtensionModel
{
  [Mono.Addins.TypeExtensionPoint(Path = Helpers.ExtensionPaths.SystemShutdownPath, NodeName = "SystemShutdownAddin")]
  public interface ISystemShutdownExtension : IBaseExtension
  {
  }
}
